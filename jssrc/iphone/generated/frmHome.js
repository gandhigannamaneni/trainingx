function addWidgetsfrmHome() {
    frmHome.setDefaultUnit(kony.flex.DP);
    var flexLogin = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "220dp",
        "id": "flexLogin",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlexBg",
        "top": "74dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flexLogin.setDefaultUnit(kony.flex.DP);
    var tbUserName = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "defTextBoxFocus",
        "height": "40dp",
        "id": "tbUserName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "20dp",
        "placeholder": "User Name",
        "secureTextEntry": false,
        "skin": "defTextBoxNormal",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "27dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "defTextBoxPlaceholder",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var tbPassword = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "defTextBoxFocus",
        "height": "40dp",
        "id": "tbPassword",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "20dp",
        "placeholder": "Password",
        "secureTextEntry": false,
        "skin": "defTextBoxNormal",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "90dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "defTextBoxPlaceholder",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var Button0h37cb913756e41 = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "defBtnFocus",
        "height": "50dp",
        "id": "Button0h37cb913756e41",
        "isVisible": true,
        "left": "16dp",
        "skin": "defBtnNormal",
        "text": "Login",
        "top": "150dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flexLogin.add(tbUserName, tbPassword, Button0h37cb913756e41);
    frmHome.add(flexLogin);
};

function frmHomeGlobals() {
    frmHome = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmHome,
        "enabledForIdleTimeout": false,
        "id": "frmHome",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFrmBg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};