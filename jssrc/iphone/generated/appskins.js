function skinsInit() {
    defBtnFocus = "defBtnFocus";
    defBtnNormal = "defBtnNormal";
    defLabel = "defLabel";
    defTextBoxFocus = "defTextBoxFocus";
    defTextBoxNormal = "defTextBoxNormal";
    defTextBoxPlaceholder = "defTextBoxPlaceholder";
    sknFlexBg = "sknFlexBg";
    sknFrmBg = "sknFrmBg";
    slDynamicNotificationForm = "slDynamicNotificationForm";
    slFbox = "slFbox";
    slForm = "slForm";
    slPopup = "slPopup";
    slStaticNotificationForm = "slStaticNotificationForm";
    slTitleBar = "slTitleBar";
    slWatchForm = "slWatchForm";
};